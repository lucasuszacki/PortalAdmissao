# Portal de Admissão

Este projeto tem como objetivo facilitar os meios de arquivação de documentos durante
a admissão de novos funcionários. Ele apresenta novas soluções para organizar os documentos
enviados pelos novos funcionários, evitando que o RH da empresa faça este processo manualmente. 
Portanto, ele automatiza o processo entre o RH e o novo funcionário.

## Instalação
1. Clone o repositório do projeto:
```
$ git clone https://gitlab.com/totvsrs/PortalAdmissao
```

2. Copie a pasta `Portal Admissão` para o workspace de seu projeto no Fluig (tipicamente `C:/workspace/`).
3. Exporte a layout `admissaoPortal` para o servidor.
4. Crie uma página e selecione o layout importado.
5. Exporte o formulário `FuturoTOTVER`.
6. Exporte o diagrama `admissao`.

## Configuração
1. Na mesma pasta em que foi exportado o formulário `FuturoTOTVER`, crie outras três pastas (tipicamente `CLT`,`ESTAGIÁRIO` e `ANEXOS RH`).
2. Entre no arquivo `authorization.js` e, na linha `122`, `144` coloque o codigo da pasta referente ao cargo de `funcionário efetivados` e, na linha `129` e `151` coloque o codigo da pasta referente ao cargo de `estagiários`.
3. Entre no arquivo `authorization.js` e, na linha `83` e `111` coloque o codigo da pasta referente `ANEXOS RH`.
4. Para configurar o OAuth acesse todas as pastas de `javascript` e procure pelo seguinte código (`ATENÇÃO: Substitua o valor dentro do apóstrofo pelo valor do campo`):
```
var key_fluig = 'VALOR' // key_fluig representa o Consumer Key
var secret_fluig = 'VALOR' // secret_fluig representa o Consumer Secret
var token_key_fluig = 'VALOR' // token_key_fluig representa o Access Token
var token_secret_fluig = 'VALOR' // token_secret_fluig representa o Token Secret
```
5. Entre no arquivo `movementProcess.js` e, na linha `143`, `144`, `332` e `333` preencha com os dados de um usuário válido.
6. Crie um usuário novo com o nome de `portal.admissão`.
7. No workflow atribua este novo usuário como mecanismo de atribuição selecione `Atribuição por Usuário` nas atividades `Preencher dados` e `Anexos`.
8. Crie um grupo no Fluig (tipicamente `RH`).
9. No workflow atribua este novo grupo como mecanismo de atribuição selecione `Atribuição por Grupo` nas atividades restantes.
10. Entre em cada template de e-mail e procure por `LINK DO PORTAL` e, coloque o link público do seu portal exportado.

## Dependências e bibliotecas
- *jQueryFileUpload* para anexar arquivos
- *crypto-js* para o sistema de criptografia
- *jquery.mask* para o sistema de máscaras

## Contribuições
- [Lucas Giacchin Uszacki](mailto:lucas.uszacki@totvs.com.br) Desenvolvedor | TOTVS Rio Grande do Sul
- [Jean Ampos Flesch](mailto:jean.flesch@totvs.com.br) Orientador | TOTVS Rio Grande do Sul